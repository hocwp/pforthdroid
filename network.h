//Copyright (c) 2011-2020 <>< Philippe Brochard - Under the MIT/x11 or NewBSD License you choose.
// NO WARRANTY! NO GUARANTEE OF SUPPORT! USE AT YOUR OWN RISK

#ifndef _NETWORK_H
#define _NETWORK_H

#ifdef NETWORK

#ifdef __cplusplus
extern "C" {
#endif

#include <errno.h>
#include "pf_all.h"

#define PERROR(msg) ERR(msg); ERR(": "); ERR(strerror(errno));

#define FAILURE 0

cell_t client_start(cell_t host, cell_t port);
cell_t server_start (cell_t port);
cell_t server_close(cell_t fd);
cell_t server_nonblocking (cell_t server_fd);
cell_t server_accept (cell_t server_fd);
cell_t socket_recv(cell_t socket);
cell_t socket_send (cell_t msg, cell_t socket);
cell_t socket_send_line (cell_t msg, cell_t socket);


#ifdef __cplusplus
}
#endif

#endif /* NETWORK */

#endif /* _NETWORK_H */
