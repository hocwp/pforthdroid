//Copyright (c) 2011-2020 <>< Philippe Brochard - Under the MIT/x11 or NewBSD License you choose.
// NO WARRANTY! NO GUARANTEE OF SUPPORT! USE AT YOUR OWN RISK

#ifndef _ANDROID_STDIO_H
#define _ANDROID_STDIO_H

#ifdef __cplusplus
extern "C" {
#endif

void placeAtXY(cell_t x, cell_t y);
void outputChar(cell_t c);

void renderScreen ( void );
void clearScreen ( void );
void renderTextScreen ( void );
void flipScreen ( void );

void reset (void);

cell_t getTsNum (void);
void setTsNum (cell_t Val);
cell_t getTsDen (void);
void setTsDen (cell_t Val);

cell_t getTextMode(void);
void setTextMode( cell_t Val );

#ifdef __cplusplus
}
#endif

#endif /* _ANDROID_STDIO_H */
