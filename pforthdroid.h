//Copyright (c) 2011-2020 <>< Philippe Brochard - Under the MIT/x11 or NewBSD License you choose.
// NO WARRANTY! NO GUARANTEE OF SUPPORT! USE AT YOUR OWN RISK

#ifndef _PFORTHDROID_H
#define _PFORTHDROID_H


#ifdef __cplusplus
extern "C" {
#endif

#define SCREENBUFSIZE 1000

#define ENTER      '\n'
#define BACKSPACE  (0x08)
#define DELETE     (0x7F)

extern int lastbuttonx;
extern int lastbuttony;
extern int lastmotionx;
extern int lastmotiony;
extern int lastbid;
extern int lastmask;
extern int lastkey, lastkeydown;

extern volatile int suspended;

extern short screenx, screeny;
extern int xoffset, yoffset;

extern int keyboard_up;

extern int tsNum, tsDen;

extern char out[SCREENBUFSIZE][127];
extern int posx, posy;

void clearOut ( void );
int checkKeyboard (void);
int queryKeyboard (void);

void toggleKeyboard (void);
cell_t getKeyboard (void);
void setKeyboard (cell_t Val);

#ifdef __cplusplus
}
#endif

#endif /* _PFORTHDROID_H */
