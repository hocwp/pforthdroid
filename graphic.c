//Copyright (c) 2011-2020 <>< Philippe Brochard - Under the MIT/x11 or NewBSD License you choose.
// NO WARRANTY! NO GUARANTEE OF SUPPORT! USE AT YOUR OWN RISK

#define CNFG3D
#include "CNFG.h"

#include "pf_all.h"
#include "pforthdroid.h"
#include "dottext.h"

#define MAX_POLY_POINTS 10

static uint32_t RGBColor;
static RDPoint polyPoints[MAX_POLY_POINTS];
static int currentPolyPoint;

cell_t getPenX (void) {
	return CNFGPenX;
}

cell_t getPenY (void) {
	return CNFGPenY;
}

void setPen(cell_t X, cell_t Y) {
	CNFGPenX = X; CNFGPenY = Y;
}

cell_t getColor (void) {
	return RGBColor;
}

void setColor(cell_t Val) {
	RGBColor = Val;
}

void drawPoint(void) {
	CNFGColor(RGBColor);
	CNFGTackPixel( CNFGPenX, CNFGPenY );
}

void drawLine(cell_t X2, cell_t Y2) {
	CNFGColor(RGBColor);
	CNFGTackSegment( CNFGPenX, CNFGPenY, X2, Y2);
}

void drawRectangle(cell_t X2, cell_t Y2) {
	CNFGColor(RGBColor);
	CNFGTackSegment( CNFGPenX, CNFGPenY, X2, CNFGPenY);
	CNFGTackSegment( CNFGPenX, Y2, X2, Y2);
	CNFGTackSegment( CNFGPenX, CNFGPenY, CNFGPenX, Y2);
	CNFGTackSegment( X2, CNFGPenY, X2, Y2);
}

void drawBox(cell_t X2, cell_t Y2) {
	CNFGColor(RGBColor);
	CNFGTackRectangle( CNFGPenX, CNFGPenY, X2, Y2);
}

void initPoly(void) {
	currentPolyPoint = 0;
}

void addToPoly(cell_t X, cell_t Y) {
	if (currentPolyPoint >= MAX_POLY_POINTS) return;

	polyPoints[currentPolyPoint].x  = X;
	polyPoints[currentPolyPoint].y  = Y;

	currentPolyPoint += 1;
}

void drawPoly(cell_t X, cell_t Y) {
	CNFGColor(RGBColor);
	addToPoly(X, Y);
	CNFGTackPoly(polyPoints, currentPolyPoint);
}

void drawText(cell_t str) {
	char buffer[1024] = {0};
	ForthStringToC( buffer, (char *) str, sizeof(buffer));
	CNFGColor(RGBColor);
	drawDotText(buffer, tsNum, tsDen);
}
