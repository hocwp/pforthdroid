\ Copyright (c) 2019-2020 <>< Philippe Brochard - Under the MIT/x11 or NewBSD License you choose.
\ NO WARRANTY! NO GUARANTEE OF SUPPORT! USE AT YOUR OWN RISK

include repl.fth

8000 constant PORT

." Starting server on port:" PORT . CR
PORT server-start
server-accept constant sock

CR ." Sending to client" CR
c" Hello from server!"     sock socket-send      drop
c"   Waiting for reply..." sock socket-send-line drop

." Waiting from client..." CR
sock socket-recv
." Reply from client:" CR
count type

REPL
