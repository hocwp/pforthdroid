\ Copyright (c) 2019-2020 <>< Philippe Brochard - Under the MIT/x11 or NewBSD License you choose.
\ NO WARRANTY! NO GUARANTEE OF SUPPORT! USE AT YOUR OWN RISK

variable done?   done? off

: readin ( -- a c)
  pad 100 accept   pad swap ;

: ok  ."   ok" CR .S ;

: bye done? on ;

: repl
  begin
	readin
	['] evaluate catch if 2drop then ok
	done? @
  until
;
