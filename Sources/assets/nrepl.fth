\ Copyright (c) 2019-2020 <>< Philippe Brochard - Under the MIT/x11 or NewBSD License you choose.
\ NO WARRANTY! NO GUARANTEE OF SUPPORT! USE AT YOUR OWN RISK

\ Start a remote repl on the specified port
\   Example:
\     On app side:    9876 nrepl
\     On client side: telnet <host> 9876
\
\ Warning: be careful since you expose an access to your device

variable sock
variable done?   done? off

: $->pad ( c-addr n -- pad-addr ) dup pad ! pad 1+ swap cmove pad ;

: .st ( c-addr --) sock @  socket-send-line drop ;
: . ( n --) dup .   s>d <# #s #> $->pad .st ;

: readin ( -- a)   sock @  socket-recv ;
: ok  ."   ok" CR .S ;

: bye done? on ;

: open-server ( port --)
  render  server-start  render
  server-accept sock !
  sock @ server-nonblocking drop
;

: evaluate-in ( a --)
  count 2dup type
  ['] evaluate catch if 2drop then ok ;

: nrepl ( port --)
  open-server  ok render
  begin
	readin ?dup if evaluate-in else 100 ms then
	render   done? @
  until
;
