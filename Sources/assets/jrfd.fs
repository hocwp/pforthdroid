\ Copyright (c) 2020 <>< Philippe Brochard - Under the MIT/x11 license
\  NO WARRANTY! NO GUARANTEE OF SUPPORT! USE AT YOUR OWN RISK

\ From: https://gitlab.com/hocwp/jrfd

\ include random.fs

: q:  [char] ? parse 2drop ;

: .OK  ." Yes :-)" ;
: .KO  ." No :-(" ;
: .??  2dup  ."    "  . ." / " . ;
: check?  <= if .OK else .KO then ;
: ?? ( n -)  100 choose cr .?? check? ;
