//Copyright (c) 2011-2020 <>< Philippe Brochard - Under the MIT/x11 or NewBSD License you choose.
// NO WARRANTY! NO GUARANTEE OF SUPPORT! USE AT YOUR OWN RISK

#ifdef REMOTE_KEYBOARD

#include <stdlib.h>
#include <sys/socket.h>
#include <network.h>
#include <unistd.h>
#include <remote_keyboard.h>

static cell_t remote_keyboard_socket = -1;
static int curPos = -1;
static char buffer[1024] = {0};

void initRemoteKeyboard(cell_t port) {
	printf("Init REMOTE Keyboard on port %d\n", (int) port);

	cell_t server_fd = server_start(port);
	remote_keyboard_socket = server_accept(server_fd);

	server_nonblocking(remote_keyboard_socket);
}

void initDefaultRemoteKeyboard(void) {
	initRemoteKeyboard(REMOTE_KEYBOARD_PORT);
}

void closeRemoteKeyboard(void) {
	if (remote_keyboard_socket == -1)  return;

	close(remote_keyboard_socket);
}

char poolRemoteKeyboard(void) {
	if (remote_keyboard_socket == -1)  return 0;

	if (curPos == -1) {
		int len = 0;
		if ((len = recv(remote_keyboard_socket , buffer, sizeof(buffer), 0)) < 0) {
			if (errno == EAGAIN) {
				curPos = -1;
				return 0;
			}
			PERROR("recv failed");
			return 0;
		}
		buffer[len] = 0;
		curPos = 0;
		return 0;
	}

	int ret;
	if (curPos != -1 && (ret = buffer[curPos]) != 0) {
		curPos += 1;
		return ret != 13 ? ret : 0;
	}

	curPos = -1;
	return 0;
}

void sendRemoteKeyboard(char c) {
	if (remote_keyboard_socket == -1) return;

	if (send(remote_keyboard_socket, &c, 1, 0) < 0) {
		PERROR("send failed");
		return;
	}
}

#endif /* REMOTE_KEYBOARD */
