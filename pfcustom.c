/* @(#) pfcustom.c 98/01/26 1.3 */

/***************************************************************
** Call Custom Functions for pForth
**
** Author: Phil Burk
** Copyright 1994 3DO, Phil Burk, Larry Polansky, David Rosenboom
** Copyright 2020 Philippe Brochard
**
** Permission to use, copy, modify, and/or distribute this
** software for any purpose with or without fee is hereby granted.
**
** THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
** WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL
** THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
** CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING
** FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
** CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
** OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
**
***************************************************************/

#include "os_generic.h"
#include "pf_all.h"
#include "pforthdroid.h"
#include "network.h"
#include "remote_keyboard.h"
#include "CNFGAndroid.h"
#include "android_stdio.h"
#include "graphic.h"

#define DEFWORD(NAME, RET, NARGS) err = CreateGlueToC( NAME, i++, RET, NARGS ); if( err < 0 ) return err;

#define M_FP_POP    (*(gCurrentTask->td_FloatStackPtr++))

static void sleep_ms ( cell_t val ) {
	usleep(val * 1000);
}

static cell_t getScreenX (void) {
	return screenx;
}

static cell_t getScreenY (void) {
	return screeny;
}

static void testFloat(cell_t val) {
	printf("\n\n========================================\n");
	printf("= VAL=%d\n", (int) val);
	printf("= float = %f\n", M_FP_POP);
	printf("========================================\n\n");
}

CFunc0 CustomFunctionTable[] = {
	(CFunc0) sleep_ms,

	(CFunc0) renderScreen,
	(CFunc0) clearScreen,
	(CFunc0) renderTextScreen,
	(CFunc0) flipScreen,

	(CFunc0) getScreenX,
	(CFunc0) getScreenY,

	(CFunc0) reset,
	(CFunc0) clearOut,
	(CFunc0) getTsNum,
	(CFunc0) setTsNum,
	(CFunc0) getTsDen,
	(CFunc0) setTsDen,

	(CFunc0) getTextMode,
	(CFunc0) setTextMode,

	(CFunc0) toggleKeyboard,
	(CFunc0) getKeyboard,
	(CFunc0) setKeyboard,

#ifdef NETWORK
	/* server-part */
	(CFunc0) client_start,
	(CFunc0) server_start,
	(CFunc0) server_close,
	(CFunc0) server_nonblocking,
	(CFunc0) server_accept,
	(CFunc0) socket_recv,
	(CFunc0) socket_send,
	(CFunc0) socket_send_line,
#endif /* NETWORK */

#ifdef REMOTE_KEYBOARD
	(CFunc0) initRemoteKeyboard,
	(CFunc0) initRemoteKeyboard,
	(CFunc0) initDefaultRemoteKeyboard,
#endif /* REMOTE_KEYBOARD */

	(CFunc0) placeAtXY,
	(CFunc0) outputChar,

	(CFunc0) getPenX,
	(CFunc0) getPenY,
	(CFunc0) setPen,

	(CFunc0) getColor,
	(CFunc0) setColor,

	(CFunc0) drawPoint,
	(CFunc0) drawLine,
	(CFunc0) drawRectangle,
	(CFunc0) drawBox,

	(CFunc0) initPoly,
	(CFunc0) addToPoly,
	(CFunc0) drawPoly,
	(CFunc0) drawText,

	(CFunc0) testFloat
};

Err CompileCustomFunctions( void )
{
	Err err;
	int i = 0;
/* Compile Forth words that call your custom functions.
** Make sure order of functions matches that in LoadCustomFunctionTable().
** Parameters for CreateGlueToC are: Name in UPPER CASE, Function, Index, Mode, NumParams
**            for DEFWORD are:       Name in UPPER CASE,                  Mode, NumParams
*/
	DEFWORD("MS", C_RETURNS_VOID, 1);

	DEFWORD("RENDER", C_RETURNS_VOID, 0 );
	DEFWORD("CLEAR", C_RETURNS_VOID, 0 );
	DEFWORD("RENDERTEXT", C_RETURNS_VOID, 0 );
	DEFWORD("FLIP", C_RETURNS_VOID, 0 );

	DEFWORD("SCREENX@", C_RETURNS_VALUE, 0 );
	DEFWORD("SCREENY@", C_RETURNS_VALUE, 0 );

	DEFWORD("RESET", C_RETURNS_VOID, 0 );
	DEFWORD("CLEAROUT", C_RETURNS_VOID, 0 );
	DEFWORD("TSNUM@", C_RETURNS_VALUE, 0 );
	DEFWORD("TSNUM!", C_RETURNS_VOID, 1 );
	DEFWORD("TSDEN@", C_RETURNS_VALUE, 0 );
	DEFWORD("TSDEN!", C_RETURNS_VOID, 1 );

	DEFWORD("TEXTMODE@", C_RETURNS_VALUE, 0 );
	DEFWORD("TEXTMODE!", C_RETURNS_VOID, 1 );

	DEFWORD("KB", C_RETURNS_VOID, 0 );
	DEFWORD("KB@", C_RETURNS_VALUE, 0 );
	DEFWORD("KB!", C_RETURNS_VOID, 1 );

#ifdef NETWORK
	/* server part */
	DEFWORD("CLIENT-START", C_RETURNS_VALUE, 2 );
	DEFWORD("SERVER-START", C_RETURNS_VALUE, 1 );
	DEFWORD("SERVER-CLOSE", C_RETURNS_VALUE, 1 );
	DEFWORD("SERVER-NONBLOCKING", C_RETURNS_VALUE, 1 );
	DEFWORD("SERVER-ACCEPT", C_RETURNS_VALUE, 1 );
	DEFWORD("SOCKET-RECV", C_RETURNS_VALUE, 1 );
	DEFWORD("SOCKET-SEND", C_RETURNS_VALUE, 2 );
	DEFWORD("SOCKET-SEND-LINE", C_RETURNS_VALUE, 2 );
#endif

#ifdef REMOTE_KEYBOARD
	DEFWORD("REMOTE-KEYBOARD", C_RETURNS_VOID, 1 );
	DEFWORD("RKB", C_RETURNS_VOID, 1 );
	DEFWORD("DRKB", C_RETURNS_VOID, 0 );
#endif /* REMOTE_KEYBOARD */

	DEFWORD("AT-XY", C_RETURNS_VOID, 2);
	DEFWORD("C.", C_RETURNS_VOID, 1);

	DEFWORD("PENX@", C_RETURNS_VALUE, 0);
	DEFWORD("PENY@", C_RETURNS_VALUE, 0);
	DEFWORD("PEN!", C_RETURNS_VOID, 2);

	DEFWORD("COLOR@", C_RETURNS_VALUE, 0);
	DEFWORD("COLOR!", C_RETURNS_VOID, 1);

	DEFWORD(".POINT", C_RETURNS_VOID, 0);
	DEFWORD(".LINE", C_RETURNS_VOID, 2);
	DEFWORD(".RECT", C_RETURNS_VOID, 2);
	DEFWORD(".BOX", C_RETURNS_VOID, 2);

	DEFWORD("<POLY", C_RETURNS_VOID, 0);
	DEFWORD("POLY+", C_RETURNS_VOID, 2);
	DEFWORD(".POLY>", C_RETURNS_VOID, 2);

	DEFWORD(".TEXT", C_RETURNS_VOID, 1);

	DEFWORD("C", C_RETURNS_VOID, 1);

	return 0;
}
