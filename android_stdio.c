//Copyright (c) 2011-2020 <>< Philippe Brochard - Under the MIT/x11 or NewBSD License you choose.
// NO WARRANTY! NO GUARANTEE OF SUPPORT! USE AT YOUR OWN RISK

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "os_generic.h"
#include <android_native_app_glue.h>
#include "CNFGAndroid.h"

#define CNFG3D
#include "CNFG.h"

#include "pf_all.h"
#include "pforthdroid.h"
#include "android_stdio.h"

#ifdef REMOTE_KEYBOARD
#include "network.h"
#include "remote_keyboard.h"
#endif /* REMOTE_KEYBOARD */

#include "virtual_keyboard.h"

#include "dottext.h"

static int atX, atY;
int textMode = 1;

void reset ( void ) {
	xoffset = 0;
	yoffset = 0;
}

cell_t getTsNum ( void ) {
	return tsNum;
}

void setTsNum ( cell_t Val ) {
	tsNum = Val;
}

cell_t getTsDen ( void ) {
	return tsDen;
}

void setTsDen ( cell_t Val ) {
	tsDen = Val;
}

cell_t getTextMode(void) {
	return textMode;
}

void setTextMode( cell_t Val ) {
	textMode = Val;
}

void placeAtXY(cell_t x, cell_t y) {
	atX = x; atY = y;
}

void outputChar(cell_t c) {
	int py = (posy - atY) % SCREENBUFSIZE;
	int i;

	for (i = 0 ; i < atX; i++) {
		if (out[py][i] == 0) out[py][i] = ' ';
	}

	out[py][atX] = (char) c;
	out[py][atX+1] = 0;
}

void clearScreen ( void ) {
	CNFGClearFrame();
	CNFGGetDimensions( &screenx, &screeny );

	CNFGColor(0x333333);
	CNFGTackRectangle(0, 0, screenx, screeny);
}

void renderTextScreen ( void ) {
	int i;
	int py = (posy + 1) % SCREENBUFSIZE;
	int pyStart;

	switch(keyboard_up) {
	case 0: pyStart = screeny;
		break;
	case 1: pyStart = screeny / 2;
		break;
#ifdef VIRTUAL_KEYBOARD
	case 2: pyStart = vkb_y1;
		break;
#endif /* VIRTUAL_KEYBOARD */
	}

	pyStart -= SCREENBUFSIZE * tsNum * 12 / tsDen;

	CNFGColor( 0xFFFFFF );
	for (i = 0; i < SCREENBUFSIZE; i++) {
		CNFGPenX = xoffset;
		CNFGPenY = pyStart + yoffset + i * tsNum * 12 / tsDen;
		if (CNFGPenY > -(tsNum * 8 / tsDen + 2) && CNFGPenY < screeny + tsNum * 12 /tsDen) {
			drawDotText( out[py], tsNum, tsDen );
		}
		py = (py + 1) % SCREENBUFSIZE;
	}

	CNFGColor( 0x00FF00 );
	py = posy % SCREENBUFSIZE;
	CNFGPenX = xoffset + tsNum * 8 * strnlen(out[py], 127) / tsDen;
	CNFGPenY = pyStart + yoffset + (SCREENBUFSIZE - 1) * tsNum * 12 / tsDen;
	drawDotText( "$", tsNum , tsDen);
}

void flipScreen ( void ) {
	CNFGFlushRender();
	CNFGSwapBuffers();

	CNFGHandleInput();
}

void renderScreen ( void )
{
	if (!textMode) {
		CNFGHandleInput();
		return;
	}

	clearScreen();

	renderTextScreen();

#ifdef VIRTUAL_KEYBOARD
	drawVirtualKeyboard();
#endif /* VIRTUAL_KEYBOARD */

	flipScreen();
}

/* Default portable terminal I/O. */
int  sdTerminalOut( char c )
{
	if (c == ENTER) {
		posy += 1;
		posx = 0;
		if (posy >= SCREENBUFSIZE) {
			posy = 0;
		}
	} else if (c == BACKSPACE || c == DELETE) {
		posx -= 1;
	} else if (posx < 127) {
		out[posy][posx] = c;
		posx += 1;
	}
	out[posy][posx] = 0;

#ifdef REMOTE_KEYBOARD
	sendRemoteKeyboard(c);
#endif /* REMOTE_KEYBOARD */

	return putchar(c);
}

int  sdTerminalIn( void )
{
	int retKey = 0;

	while(1) {
		if( suspended ) { usleep(500000); continue; }

#ifdef REMOTE_KEYBOARD
		if ((retKey = poolRemoteKeyboard()) != 0) {
			return retKey;
		}
#endif /* REMOTE_KEYBOARD */

#ifdef VIRTUAL_KEYBOARD
		if ((retKey = checkVirtualKeyboard()) != 0) {
			return retKey;
		}
#endif /* VIRTUAL_KEYBOARD */

		if ((retKey = checkKeyboard()) != 0) {
			return retKey;
		}

		renderScreen();

		usleep(5000);
	}
}

/* We need to echo because getchar() don't. */
int  sdTerminalEcho( char c )
{
	sdTerminalOut(c);
	return 0;
}

int  sdQueryTerminal( void )
{
	renderScreen();

#ifdef VIRTUAL_KEYBOARD
	return queryKeyboard() || queryVirtualKeyboard();
#else
	return queryKeyboard();
#endif /* VIRTUAL_KEYBOARD */
}

int  sdTerminalFlush( void )
{
	return 0;
}

void sdTerminalInit( void )
{
}
void sdTerminalTerm( void )
{
}
