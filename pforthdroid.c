//Copyright (c) 2011-2020 <>< Charles Lohr - Under the MIT/x11 or NewBSD License you choose.
//Copyright (c) 2020 <>< Philippe Brochard - Under the MIT/x11 or NewBSD License you choose.
// NO WARRANTY! NO GUARANTEE OF SUPPORT! USE AT YOUR OWN RISK

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "os_generic.h"
#include <GLES3/gl3.h>
#include <asset_manager.h>
#include <asset_manager_jni.h>
#include <android_native_app_glue.h>
#include <android/sensor.h>
#include "CNFGAndroid.h"

#include "pf_all.h"
#include "pforthdroid.h"
#include <dirent.h>

#define CNFG_IMPLEMENTATION
#define CNFG3D
#include "CNFG.h"

#include <remote_keyboard.h>
#include <virtual_keyboard.h>


extern struct android_app * gapp;

short screenx, screeny;
int xoffset = 0, yoffset = 0;

int tsNum = 8, tsDen = 5;

volatile int suspended;

unsigned frames = 0;
unsigned long iframeno = 0;

void AndroidDisplayKeyboard(int pShow);

int lastbuttonx = 0;
int lastbuttony = 0;
int lastmotionx = 0;
int lastmotiony = 0;
int lastbid = 0;
int lastmask = 0;
int lastkey, lastkeydown;

int keyboard_up = 1;

char out[SCREENBUFSIZE][127];
int posx = 0, posy = 0;

int checkKeyboard (void) {
	if (lastkey != 0 && lastkeydown == 1) {
		if (lastkey == 67) lastkey = BACKSPACE;
		lastkeydown = 0;
		return lastkey;
	}
	return 0;
}

int queryKeyboard (void) {
	return lastkey != 0 && lastkeydown == 1;
}

void toggleKeyboard ( void ) {
#ifdef VIRTUAL_KEYBOARD
#define MODKEYBOARD 3
#else
#define MODKEYBOARD 2
#endif /* VIRTUAL_KEYBOARD */

	keyboard_up = (keyboard_up + 1) % MODKEYBOARD;
	AndroidDisplayKeyboard( keyboard_up == 1);
}

cell_t getKeyboard (void) {
	return keyboard_up;
}

void setKeyboard (cell_t Val) {
	keyboard_up = Val;
	AndroidDisplayKeyboard( keyboard_up == 1);
}

void HandleKey( int keycode, int bDown )
{
	lastkey = keycode;
	lastkeydown = bDown;
}

void HandleButton( int x, int y, int button, int bDown )
{
	int dx = x - lastbuttonx;
	int dy = y - lastbuttony;

	lastbid = button;
	lastbuttonx = x;
	lastbuttony = y;

	if (bDown) {
		lastmotionx = x;
		lastmotiony = y;
	}

#ifdef VIRTUAL_KEYBOARD
	if (handleVirtualKeyboard(x, y, button, bDown)) return;
#endif /* VIRTUAL_KEYBOARD */

	if( !bDown && abs(dx) < 10 && abs(dy) < 10) {
		toggleKeyboard();
	}
}

void HandleMotion( int x, int y, int mask )
{
#ifdef VIRTUAL_KEYBOARD
	if (inVirtualKeybord(x,y)) return;
#endif /* VIRTUAL_KEYBOARD */

	int dx = x - lastmotionx;
	int dy = y - lastmotiony;

	lastmask = mask;
	lastmotionx = x;
	lastmotiony = y;

	xoffset += dx;
	yoffset += dy;
}

void HandleDestroy()
{
	printf( "Destroying\n" );
	exit(10);
}

void HandleSuspend()
{
	suspended = 1;
}

void HandleResume()
{
	suspended = 0;
}

int listDir (const char * dirname) {
	DIR *dir;
	struct dirent *ent;

	printf("\n**Listing %s ******************\n", dirname);

	if ((dir = opendir (dirname)) != NULL) {
		/* print all the files and directories within directory */
		while ((ent = readdir (dir)) != NULL) {
			printf ("%s\n", ent->d_name);
		}
		closedir (dir);
	} else {
		/* could not open directory */
		perror ("");
		return EXIT_FAILURE;
	}
	printf("--------------------\n");
	return 0;
}

void installFileFromAsset (const char *filename)
{
	const char * asset = "Not Found";
	AAsset * asset_file = AAssetManager_open( gapp->activity->assetManager, filename, AASSET_MODE_BUFFER );
	if( asset_file )
	{
		size_t fileLength = AAsset_getLength(asset_file);
		char * temp = malloc( fileLength + 1);
		memcpy( temp, AAsset_getBuffer( asset_file ), fileLength );
		temp[fileLength] = 0;
		asset = temp;

		FILE * fp=fopen(filename, "w");
		fprintf(fp, "%s", asset);
		fclose(fp);
	}
}

void clearOut ( void ) {
	int i;

	for (i = 0; i < SCREENBUFSIZE; i++) {
		out[i][0] = 0;
	}
}

void cleanSourceFiles(void) {
	FILE * fp = fopen("sources-to-remove.txt", "r");
	if (fp) {
		char *filename = NULL;
		size_t len = 0;
		ssize_t read;

		while ((read = getline(&filename, &len, fp)) != -1) {
			printf("Deleting source file: %s", filename);
			filename[read - 1] = 0;
			unlink(filename);
		}

		free(filename);
		fclose(fp);
	}
}

void launchPForth ( void )
{
	char cwd[1024];

	clearOut();

	printf("\n\n\n== PForthDroid started ====================================================================\n\n\n");

	chdir(PFORTHDROIDFILES);
	getcwd(cwd, sizeof(cwd));
	printf("Current working dir: %s\n", cwd);

	listDir(".");

	FILE * fp = fopen("pforth.dic", "r");
	if (fp) {
		printf("Found pforth.dic: no need to rebuild the dictionary!\n");
		fclose(fp);
	} else {
		printf("pforth.dic not found: a dictionary rebuild is needed!\n");

		AAssetDir * assetDir = AAssetManager_openDir(gapp->activity->assetManager, "");
		const char * assetFilename = (const char*)NULL;;
		while ((assetFilename = AAssetDir_getNextFileName(assetDir)) != NULL) {
			installFileFromAsset(assetFilename);
		}
		AAssetDir_close(assetDir);

		listDir(".");

		pfDoForth(NULL, "system.fth", 1);

		cleanSourceFiles();
	}

	listDir(".");

	pfDoForth("pforth.dic", SOURCE_NAME, 0); // OK RUN

	printf("\n\n\n-- PForthDroid ended --------------------------------------------------------------------\n\n\n");
}

int main()
{
	CNFGBGColor = 0x000000;
	CNFGSetupFullscreen( "PForthDroid", 0 );
	//CNFGSetup( "PForthDroid", -1, -1 );

	CNFGGetDimensions( &screenx, &screeny );
	tsDen = 10;
	tsNum = (screenx * tsDen / (45 * 8));

#ifdef VIRTUAL_KEYBOARD
	keyboard_up = 0;
	keyboard_up = 2;
	initVirtualKeyboard();
#endif

	AndroidDisplayKeyboard( keyboard_up == 1 );

	launchPForth();

#ifdef REMOTE_KEYBOARD
	closeRemoteKeyboard();
#endif /* REMOTE_KEYBOARD */

	AndroidSendToBack( 1 );

	return(0);
}
