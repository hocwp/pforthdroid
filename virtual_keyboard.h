//Copyright (c) 2011-2020 <>< Philippe Brochard - Under the MIT/x11 or NewBSD License you choose.
// NO WARRANTY! NO GUARANTEE OF SUPPORT! USE AT YOUR OWN RISK

#ifndef _VIRTUAL_KEYBOARD_H
#define _VIRTUAL_KEYBOARD_H

#ifdef VIRTUAL_KEYBOARD

#ifdef __cplusplus
extern "C" {
#endif

extern int vkb_tsNum;
extern int vkb_tsDen;

extern int vkb_x1, vkb_y1, vkb_x2, vkb_y2;


void initVirtualKeyboard(void);
void drawVirtualKeyboard(void);
int handleVirtualKeyboard(int x, int y, int button, int bDown);
int inVirtualKeybord (int x, int y);
int checkVirtualKeyboard(void);
int queryVirtualKeyboard(void);

#ifdef __cplusplus
}
#endif

#endif /* VIRTUAL_KEYBOARD */

#endif /* _VIRTUAL_KEYBOARD_H */
