//Copyright (c) 2011-2020 <>< Philippe Brochard - Under the MIT/x11 or NewBSD License, or any GPL license you choose.
// NO WARRANTY! NO GUARANTEE OF SUPPORT! USE AT YOUR OWN RISK

#ifndef _DOTTEXT
#define _DOTTEXT

#ifdef __cplusplus
extern "C" {
#endif

void drawDotChar(char c, int tsNum, int tsDen);
void drawDotText(const char * str, int tsNum, int tsDen);

#ifdef __cplusplus
}
#endif

#endif /* _DOTTEXT */
