//Copyright (c) 2011-2020 <>< Philippe Brochard - Under the MIT/x11 or NewBSD License you choose.
// NO WARRANTY! NO GUARANTEE OF SUPPORT! USE AT YOUR OWN RISK

#ifdef VIRTUAL_KEYBOARD

#include <stdlib.h>
#include "CNFG.h"
#include "pf_all.h"
#include "pforthdroid.h"
#include <dottext.h>
#include <virtual_keyboard.h>

int vkb_tsNum = 3;
int vkb_tsDen = 1;

int vkb_x1 = 0, vkb_y1 = 0, vkb_x2 = 100, vkb_y2 = 100;

static short vkb_state;
static int lastvkbkey = 0;

#define VKB_ROW 4
#define VKB_COL 7

static int special_chars[] = {'v', '1', 'A', '$', ' ', 'R', '<'};

static int layout[][VKB_ROW][VKB_COL] = {
/* LOWERCASE */
#ifdef VIRTUAL_KEYBOARD_DVORAK
	{
		{'p', 'y', 'f', 'i', 'g', 'c', 'r'},
		{'a', 'o', 'e', 'u', 'h', 't', 'n'},
		{'q', 'b', 'x', 'd', 'l', 's', 'v'},
		{-1 , 'k', 'm', 'w', 'z', 'j', ' '}
	},
#elif defined(VIRTUAL_KEYBOARD_QWERTY)
	{
		{'q', 'w', 'e', 'r', 't', 'y', 'u'},
		{'a', 's', 'd', 'f', 'g', 'h', 'j'},
		{'z', 'x', 'c', 'v', 'b', 'n', 'i'},
		{-1 , 'o', 'p', 'k', 'l', 'm', ' '}
	},
#else
	{
		{'a', 'b', 'c', 'd', 'e', 'f', 'g'},
		{'h', 'i', 'j', 'k', 'l', 'm', 'n'},
		{'o', 'p', 'q', 'r', 's', 't', 'u'},
		{-1 , 'v', 'w', 'x', 'y', 'z', ' '}
	},
#endif /* end LOWERCASE */
	{
		{'\\', '"', '1', '2', '3', ':', -6  },
		{'+',  '-', '4', '5', '6', ';', '!' },
		{'*',  '/', '7', '8', '9', '.', '@'},
		{ 0 ,  -2 , -3 , '0', '?', ' ', -5  }
	},
/* UPPERCASE */
#ifdef VIRTUAL_KEYBOARD_DVORAK
	{
		{'P', 'Y', 'F', 'I', 'G', 'C', 'R'},
		{'A', 'O', 'E', 'U', 'H', 'T', 'N'},
		{'Q', 'B', 'X', 'D', 'L', 'S', 'V'},
		{ 0 , 'K', 'M', 'W', 'Z', 'J', ' '}
	},
#elif defined(VIRTUAL_KEYBOARD_QWERTY)
	{
		{'Q', 'W', 'E', 'R', 'T', 'Y', 'U'},
		{'A', 'S', 'D', 'F', 'G', 'H', 'J'},
		{'Z', 'X', 'C', 'V', 'B', 'N', 'I'},
		{0 , 'O', 'P', 'K', 'L', 'M', ' '}
	},
#else
	{
		{'A', 'B', 'C', 'D', 'E', 'F', 'G'},
		{'H', 'I', 'J', 'K', 'L', 'M', 'N'},
		{'O', 'P', 'Q', 'R', 'S', 'T', 'U'},
		{ 0 , 'V', 'W', 'X', 'Y', 'Z', ' '}
	},
#endif /* end UPPERCASE */
	{
		{'|', '^', '&', '<', '>', '_', -6 },
		{'~', ',', '#', '(', ')', '$', '!'},
		{'`', '%', '@', '[', ']', '.', '='},
		{ 0 , -2 , -1 , '{', '}', '\'', -5 }
	}
};


void initVirtualKeyboard(void) {
	short screenx, screeny;

	CNFGGetDimensions( &screenx, &screeny );

	vkb_x1 = 1; vkb_y1 = 2 * screeny / 3;
	vkb_x2 = screenx - 1; vkb_y2 = screeny;

	vkb_state = 0;
}

void drawVirtualKeyboard(void) {
	int i, j, ch;
	char str[2];
	float dy = (vkb_y2 - vkb_y1) / VKB_ROW;
	float dx = (vkb_x2 - vkb_x1) / VKB_COL;

	if (keyboard_up != 2) return;

	CNFGColor( 0x000000 );
	CNFGTackRectangle( vkb_x1, vkb_y1, vkb_x2, vkb_y2);

	str[1] = 0;
	for (j = 0; j < VKB_ROW; j++) {
		for (i = 0 ; i < VKB_COL; i++) {
			CNFGColor( 0X444444 );
			CNFGTackRectangle(vkb_x1 + i * dx + 2, vkb_y1 + j * dy + 2,
							  vkb_x1 + (i+1) * dx - 2, vkb_y1 + (j+1) * dy - 2);

			CNFGPenX = vkb_x1 + i * dx + (dx-vkb_tsNum*8/vkb_tsDen) / 2;
			CNFGPenY = vkb_y1 + j * dy + (dy-vkb_tsNum*8/vkb_tsDen) / 2;
			ch = layout[vkb_state][j][i];
			if (ch > 0) {
				CNFGColor( 0x00FF00 );
				str[0] = ch;
			} else {
				CNFGColor( 0x0000FF );
				str[0] = special_chars[-ch];
			}
			drawDotText( str, vkb_tsNum, vkb_tsDen );
		}
	}
}

int inVirtualKeybord (int x, int y) {
	if (keyboard_up != 2) return 0;

	return (x >= vkb_x1 && x <= vkb_x2 && y >= vkb_y1 && y <= vkb_y2);
}

int handleVirtualKeyboard(int x, int y, int button, int bDown) {
	lastvkbkey = 0;

	if (!inVirtualKeybord(x, y)) return 0;

	if (bDown) return 1;

	int col = (x - vkb_x1) / ((vkb_x2 - vkb_x1) / VKB_COL);
	int row = (y - vkb_y1) / ((vkb_y2 - vkb_y1) / VKB_ROW);

	int ch = layout[vkb_state][row][col];

	if (ch > 0) {
		lastvkbkey = ch;
		return 1;
	}

	switch(ch) {
	case 0: vkb_state = 0; break;
	case -1: vkb_state = 1; break;
	case -2: vkb_state = 2; break;
	case -3: vkb_state = 3; break;
	case -5: vkb_state = 0; lastvkbkey = ENTER; break;
	case -6: lastvkbkey = BACKSPACE; break;
	}

	return 1;
}

int checkVirtualKeyboard(void) {
	if (keyboard_up != 2) return 0;

	int retkey = lastvkbkey;
	lastvkbkey = 0;
	return retkey;
}

int queryVirtualKeyboard(void) {
	if (keyboard_up != 2) return 0;

	return lastvkbkey != 0;
}

#endif /* VIRTUAL_KEYBOARD */
