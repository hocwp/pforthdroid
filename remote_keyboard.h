//Copyright (c) 2011-2020 <>< Philippe Brochard - Under the MIT/x11 or NewBSD License you choose.
// NO WARRANTY! NO GUARANTEE OF SUPPORT! USE AT YOUR OWN RISK

#ifndef _REMOTE_KEYBOARD_H
#define _REMOTE_KEYBOARD_H

#ifdef REMOTE_KEYBOARD

#ifdef __cplusplus
extern "C" {
#endif

void initRemoteKeyboard(cell_t port);
void initDefaultRemoteKeyboard(void);
void closeRemoteKeyboard(void);
char poolRemoteKeyboard(void);
void sendRemoteKeyboard(char c);

#ifdef __cplusplus
}
#endif

#endif /* REMOTE_KEYBOARD */

#endif /* _REMOTE_KEYBOARD_H */
