//Copyright (c) 2011-2020 <>< Philippe Brochard - Under the MIT/x11 or NewBSD, or any GPL license License you choose.
// NO WARRANTY! NO GUARANTEE OF SUPPORT! USE AT YOUR OWN RISK

#include <stdlib.h>
#include <dottext.h>
#include <font8x8.h>
#include "CNFG.h"

void drawDotChar(char c, int tsNum, int tsDen) {
	const char *bitmap = font8x8[(int) c];

	int j,i;
	for (j=0; j < 8; j++) {
		for (i=0; i < 8; i++) {
			if (bitmap[j] & 1 << i) {
				CNFGTackRectangle(CNFGPenX + tsNum * i / tsDen, CNFGPenY + tsNum * j / tsDen,
								  CNFGPenX + tsNum * (i+1) / tsDen, CNFGPenY + tsNum* (j+1) / tsDen);
			}
		}
	}
}

void drawDotText(const char * str, int tsNum, int tsDen) {
	int i = 0, c;
	int startX = CNFGPenX;
	while((c = str[i])) {
		CNFGPenX = startX + tsNum * 8 * i / tsDen;
		drawDotChar(str[i], tsNum, tsDen);
		i+=1;
	}
}
