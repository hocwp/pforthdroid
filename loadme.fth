\ This file will be loaded at installation time
\ The word below will be added to the dictionary
\ Some other files can be configured in Sources/assets/ and the Makefile

: hw   cr cr ." Hello world!" cr cr ;

variable done?
: tt
  done? off
  begin
	key? if key dup  ." key=" .
			[char] a = if done? on then
		 else ." ." then
	cr 5 ms
	done? @ until ;

: waitkey  begin key? if key drop exit then again ;

: t   cls
	  0   0 at-xy [char] a c.
	  0  10 at-xy [char] b c.
	  1  10 at-xy [char] b c.
	  2  10 at-xy [char] b c.
	  4  10 at-xy [char] b c.
	  10  0 at-xy [char] c c.
	  10 10 at-xy [char] d c.
	  waitkey
;

: draw
  $00FF00 color!
  100 100 pen! .point
  150 150 pen!
  300 300 .rect
  300 300 .line
  100 310 pen! 400 500 .box
  $0000FF color!
  110 320 pen! 390 490 .box
  $F09090 color!
  <poly 50 400 poly+  100 350 poly+ 200 400 .poly>
  <poly 0 200 poly+ 100 200 poly+ 100 100 .poly>

  $00FFF0 color!
  7 tsnum! 2 tsden!
  50 300 pen! c" Hello world!" .text

  13 tsnum! 3 tsden!
  25 350 pen! c" Hello world!" .text
;

: aa
  tsNum@ tsDen@ kb@
  0 kb! 0 textmode!
  clear draw flip waitkey
  1 textmode!
  kb! tsDen! tsNum! ;
