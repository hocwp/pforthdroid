//Copyright (c) 2011-2020 <>< Philippe Brochard - Under the MIT/x11 or NewBSD License you choose.
// NO WARRANTY! NO GUARANTEE OF SUPPORT! USE AT YOUR OWN RISK

#ifndef _GRAPHIC_H
#define _GRAPHIC_H

#ifdef __cplusplus
extern "C" {
#endif

cell_t getPenX (void);
cell_t getPenY (void);
void setPen(cell_t X, cell_t Y);

cell_t getColor (void);
void setColor(cell_t Val);

void drawPoint(void);
void drawLine(cell_t X2, cell_t Y2);
void drawRectangle(cell_t X2, cell_t Y2);
void drawBox(cell_t X2, cell_t Y2);

void initPoly(void);
void addToPoly(cell_t X, cell_t Y);
void drawPoly(cell_t X, cell_t Y);

void drawText(cell_t str);

#ifdef __cplusplus
}
#endif

#endif /* _GRAPHIC_H */
